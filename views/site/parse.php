<?php

use miloschuman\highcharts\Highcharts;
use yii\widgets\ActiveForm;

?>

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    Изменение баланса
                </div>
            </div>
            <div class="panel-body" style="padding: 0px">
                <?php
                echo Highcharts::widget([
                    'scripts' => [
                        'modules/no-data-to-display',
                    ],
                    'setupOptions' => [
                        'lang' => [
                            'noData' => 'Данные не были загружены!<br/>Загрузите данные для построения графика',
                            'rangeSelectorFrom' => false,
                            'rangeSelectorTo' => false,
                            'rangeSelectorZoom' => null,
                            'shortMonths' => ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
                            'weekdays' => ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
                        ],
                        'noData' => [
                            'style' => [
                                'fontWeight' => 'bold',
                                'fontSize' => '15px',
                                'color' => '#303030'
                            ]
                        ]
                    ],
                    'options' => [
                        'exporting' => false,
                        'credits' => false,
                        'title' => [
                            'text' => null,
                        ],
                        'chart' => [
                            'height' => 500,
                            'type' => 'areaspline'
                        ],
                        'xAxis' => [
                            'type' => 'datetime',
                            'categories' => array_column($model->data, 'datetime'),
                            'labels' => [
                                'rotation' => 270,
                            ],
                        ],
                        'yAxis' => [
                            'labels' => [
                                'align' => 'right',
                            ],
                            'tickInterval' => '50',
                            'title' => [
                                'text' => 'USD'
                            ]
                        ],
                        'tooltip' => [
                            'split' => true,
                            'headerFormat' => '<span style="color:{point.color}"></span> Дата: <b>{point.x}</b><br/>',
                            'pointFormat' => '<span style="color:{point.color}"></span> {series.name}: <b>{point.y}</b><br/>'
                        ],
                        'series' => [
                            [
                                'showInLegend' => false,
                                'name' => 'Баланс',
                                'data' => array_column($model->data, 'value')
                            ],
                        ]
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'file')->fileInput() ?>

    <button>Загрузить</button>

<?php ActiveForm::end() ?>