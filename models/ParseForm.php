<?php

namespace app\models;

use DateTime;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ParseForm extends Model
{
    /**
     * @var UploadedFile
     *
     */
    public $file;
    public $data = [];
    private $datetimeFormat = '';

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'html'],
            [['data'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Загрузить файл'
        ];
    }


    /**
     * Parsing Method
     * @return array|bool $data
     * @throws \yii\base\InvalidConfigException
     */

    public function parse()
    {
        $this->file = UploadedFile::getInstance($this, 'file');

        if ($this->file && $this->validate()) {
            $content = file_get_contents($this->file->tempName);
            $dom = new \DOMDocument;
            @$dom->loadHTML($content);
            $rows = $dom->getElementsByTagName('tr');
            $i = 0;
            foreach ($rows as $rowId => $row) {
                $cols = $row->getElementsByTagName('td');
                $size = count($cols);
                foreach ($cols as $colId => $col) {
                    if ($col->nodeValue === 'buy' || $col->nodeValue === 'balance' || $col->nodeValue === 'sell') {
                        if (preg_match_all('/^[\d]{4}.[\d]{2}.[\d]{2} [\d]{2}:[\d]{2}:[\d]{2}$/', $cols[1]->nodeValue)) {
                            $this->datetimeFormat = 'Y.n.j H:i:s';
                        } elseif (preg_match_all('/^[\d]{4}.[\d]{2}.[\d]{2} [\d]{2}:[\d]{2}$/', $cols[1]->nodeValue)) {
                            $this->datetimeFormat = 'Y.n.j H:i';
                        }
                        $this->data[$i]['datetime'] = Yii::$app->formatter->asDatetime($date = DateTime::createFromFormat($this->datetimeFormat, $cols[1]->nodeValue));
                        if ($i == 0) {
                            $this->data[$i]['value'] = (float)str_replace(" ", "", $cols[$size - 1]->nodeValue);
                        } else {
                            $this->data[$i]['value'] = round($this->data[$i - 1]['value'] + (float)str_replace(" ", "", $cols[$size - 1]->nodeValue), '2');
                        }
                        $i++;
                    }
                }
            }
            if (empty($this->data)) {
                return false;
            }
        }
        return $this->data;
    }

}